import React, { Component } from 'react'
import './MonthSelector.css'

class MonthsSelector extends Component {
   
        constructor(props) {
            super(props)
        
            this.state = {
                 monthselected: 'Select'
            }
        }

        monthHandleChange = event => {
            this.setState({
                monthselected: event.target.value
            })
        }

        onSubmitMonth = event => {
            alert(`You have selected ${this.state.monthselected}`)
            event.preventDefault()
        }


        render() {
        return (
            <div>
                <form onSubmit ={this.onSubmitMonth}>
                    <div>
                        <div className="titleName">Select the month</div> 
                <select className="dropdown" value = {this.state.monthselected} name="startMonth" onChange={this.monthHandleChange}>
                                <option className="dropdown-item" value="Select" disabled>Select</option>
                                <option className="dropdown-item" value="Jan">Jan</option>
                                <option className="dropdown-item" value="Feb">Feb</option>
                                <option className="dropdown-item" value="Mar">Mar</option>
                                <option className="dropdown-item" value="Apr">Apr</option>
                                <option className="dropdown-item" value="May">May</option>
                                <option className="dropdown-item" value="Jun">Jun</option>
                                <option className="dropdown-item" value="Aug">Aug</option>
                                <option className="dropdown-item" value="Sep">Sep</option>
                                <option className="dropdown-item" value="Oct">Oct</option>
                                <option className="dropdown-item" value="Nov">Nov</option>
                                <option className="dropdown-item" value="Dec">Dec</option>
                            </select>     
                  <button type="submit" className= 'btn'>Submit</button>          
                    </div>
                  </form>
            </div>

        )
    }
}

export default MonthsSelector

    
